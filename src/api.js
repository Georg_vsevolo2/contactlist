import axios from 'axios'

/**
 * @var {Axios}
 */
const instance = axios.create({
  // baseURL: process.env.API_URL
  baseURL: 'https://demo.sibers.com/'
})

/**
 * Load payments data.
 *
 * @param {Object} params
 * @returns {Promise}
 */
function getUsers (params = {}) {
  return instance.request({
    method: 'get',
    url: params.url,
    params
  })
}

export default {
  instance,
  getUsers
}
