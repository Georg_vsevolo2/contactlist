import Vue from 'vue'

import AppHeader from './AppHeader.vue'
import AppContent from './AppContent.vue'

Vue.component('app-header', AppHeader)
Vue.component('app-content', AppContent)
