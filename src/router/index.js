import Vue from 'vue'
import Router from 'vue-router'

export const ROUTE_LIST = 'userList'
export const ROUTE_USER = 'userPage'
// export const ROUTE_404 = 'route_404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: ROUTE_LIST,
      component: () => import('@/components/AppContent'),
      children: [
        {
          name: ROUTE_USER,
          path: ':id',
          component: () => import('@/components/AppContent')
        }
      ]
    }
  ]
})
