import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api'

Vue.use(Vuex)

function setLocalUsers ({ state, commit }) {
  const users = JSON.stringify(state['users'])
  localStorage.setItem('users', users)
}

function getLocalUsers () {
  const users = localStorage.getItem('users')
  if (users) {
    return Array.from(JSON.parse(users))
  }
  return []
}

export default new Vuex.Store({

  state: () => ({
    users: [],
    usersData: new Map(),
    isLoading: false
  }),
  // getters: {
  //   users: state => Array.from(state['usersData'].values())
  // },
  actions: {
    async load ({commit, state}, params = {}) {
      commit('setState', {isLoading: true})
      try {
        const users = getLocalUsers()

        if (users.length > 0) {
          commit('setUsers', users)
        } else {
          await api.getUsers(params)
            .then((res) => {
              commit('setUsers', res.data)
            }).then(() => {
              setLocalUsers({state})
            })
        }
      } catch (e) {
        alert(e)
      } finally {
        commit('setState', {isLoading: false})
      }
    },
    changeUser ({commit, state}, user) {
      commit('changeUser', user)
      setLocalUsers({state})
    },
    deleteUser ({commit, state}, id) {
      console.log('locked')
      // const user = state['users'][id]
      // commit('deleteUser', user)
      // setLocalUsers({state})
    }
  },

  mutations: {
    setUsers (state, data) {
      state['users'] = data
    },
    changeUser (state, user) {
      state['users'][user.id] = user
    },
    deleteUser (state, user) {
      state['users'] = state['users'].filter(user_ => user_.id !== user.id)
    },
    setState (state, value) {
      Object.entries(value).forEach(([key, data]) => {
        if (!Array.isArray(state[key]) && state[key] && typeof state[key] === 'object') {
          state[key] = {
            ...state[key],
            ...data
          }
        } else {
          state[key] = data
        }
      })
    }
  }
})
